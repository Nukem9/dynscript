#include "stdafx.h"

int pluginHandle;
HWND hwndDlg;
int hMenu;

Script::ModuleDef def;

void InitDebugCallback(CBTYPE Type, PLUG_CB_INITDEBUG *Info)
{
	std::string file(Info->szFileName);

	// void OnInitDebug(string &in File)
	asExecuteDynamic(def.asOnInitDebug, (OBJECT)&file);
}

void StopDebugCallback(CBTYPE Type, PLUG_CB_STOPDEBUG *Info)
{
	// void OnStopDebug()
	asExecuteDynamic(def.asOnStopDebug);
}

void CreateProcessCallback(CBTYPE Type, PLUG_CB_CREATEPROCESS *Info)
{
	// void OnCreateProcess(CREATE_PROCESS_DEBUG_INFO &in Info, IMAGEHLP_MODULE64 &in Module, PROCESS_INFORMATION &in ProcessInfo)
	asExecuteDynamic(def.asOnCreateProcess, (OBJECT)Info->CreateProcessInfo, (OBJECT)Info->modInfo, (OBJECT)Info->fdProcessInfo);
}

void ExitProcessCallback(CBTYPE Type, PLUG_CB_EXITPROCESS *Info)
{
	// void OnExitProcess(EXIT_PROCESS_DEBUG_INFO &in Info)
	asExecuteDynamic(def.asOnExitProcess, (OBJECT)Info->ExitProcess);
}

void CreateThreadCallback(CBTYPE Type, PLUG_CB_CREATETHREAD *Info)
{
	// void OnCreateThread(CREATE_THREAD_DEBUG_INFO &in Info, uint ThreadId)
	asExecuteDynamic(def.asOnCreateThread, (OBJECT)Info->CreateThread, Info->dwThreadId);
}

void ExitThreadCallback(CBTYPE Type, PLUG_CB_EXITTHREAD *Info)
{
	// void OnExitThread(EXIT_THREAD_DEBUG_INFO &in Info, uint ThreadId)
	asExecuteDynamic(def.asOnExitThread, (OBJECT)Info->ExitThread, Info->dwThreadId);
}

void SystemBreakpointCallback(CBTYPE Type, PLUG_CB_SYSTEMBREAKPOINT *Info)
{
	// void OnSystemBreakpoint()
	asExecuteDynamic(def.asOnSystemBreakpoint);
}

void LoadDllCallback(CBTYPE Type, PLUG_CB_LOADDLL *Info)
{
	std::string modname(Info->modname);

	// void OnLoadDll(LOAD_DLL_DEBUG_INFO &in Info, IMAGEHLP_MODULE64 &in Module, string &in ModName)
	asExecuteDynamic(def.asOnLoadDll, (OBJECT)Info->LoadDll, (OBJECT)Info->modInfo, (OBJECT)&modname);
}

void UnloadDllCallback(CBTYPE Type, PLUG_CB_UNLOADDLL *Info)
{
	// void OnUnloadDll(UNLOAD_DLL_DEBUG_INFO &in Info)
	asExecuteDynamic(def.asOnUnloadDll, (OBJECT)Info->UnloadDll);
}

void DebugStringCallback(CBTYPE Type, PLUG_CB_OUTPUTDEBUGSTRING *Info)
{
	//
	// Convert from unicode to multi-byte if needed
	//
	char buffer[2048];
	memset(buffer, 0, sizeof(buffer));

	size_t maxlen = min(ARRAYSIZE(buffer) - 1, Info->DebugString->nDebugStringLength);

	if (Info->DebugString->fUnicode)
	{
		wchar_t wcharbuf[2048];

		if (DbgMemRead((duint)Info->DebugString->lpDebugStringData, (PUCHAR)wcharbuf, maxlen * sizeof(wchar_t)))
			wcstombs(buffer, wcharbuf, ARRAYSIZE(buffer));
	}
	else
	{
		DbgMemRead((duint)Info->DebugString->lpDebugStringData, (PUCHAR)buffer, maxlen);
	}

	//
	// Convert to script object
	//
	std::string message(buffer);

	// void OnOutputDebugString(OUTPUT_DEBUG_STRING_INFO &in Info, string &in Message)
	asExecuteDynamic(def.asOnOutputDebugString, (OBJECT)Info->DebugString, (OBJECT)&message);
}

void ExceptionCallback(CBTYPE Type, PLUG_CB_EXCEPTION *Info)
{
	// void OnException(EXCEPTION_DEBUG_INFO &in Info)
	asExecuteDynamic(def.asOnException, (OBJECT)Info->Exception);
}

void BreakpointCallback(CBTYPE Type, PLUG_CB_BREAKPOINT *Info)
{
	// void OnBreakpoint(BRIDGEBP &in Info)
	asExecuteDynamic(def.asOnBreakpoint, (OBJECT)Info->breakpoint);
}

void PauseCallback(CBTYPE Type, PLUG_CB_PAUSEDEBUG *Info)
{
	// void OnPauseDebug()
	asExecuteDynamic(def.asOnPauseDebug);
}

void ResumeCallback(CBTYPE Type, PLUG_CB_RESUMEDEBUG *Info)
{
	// void OnResumeDebug()
	asExecuteDynamic(def.asOnResumeDebug);
}

void SteppedCallback(CBTYPE Type, PLUG_CB_STEPPED *Info)
{
	// void OnStepped()
	asExecuteDynamic(def.asOnStepped);
}

void AttachCallback(CBTYPE Type, PLUG_CB_ATTACH *Info)
{
	// void OnAttach(uint ProcessId)
	asExecuteDynamic(def.asOnAttach, Info->dwProcessId);
}

void DetachCallback(CBTYPE Type, PLUG_CB_DETACH *Info)
{
	// void OnDetach(PROCESS_INFORMATION &in Info)
	asExecuteDynamic(def.asOnDetach, (OBJECT)Info->fdProcessInfo);
}

void DebugEventCallback(CBTYPE Type, PLUG_CB_DEBUGEVENT *Info)
{
	// void OnDebugEvent(DEBUG_EVENT &in Info)
	asExecuteDynamic(def.asOnDebugEvent, (OBJECT)Info->DebugEvent);
}

void MenuEntryCallback(CBTYPE Type, PLUG_CB_MENUENTRY *Info)
{
	// void OnMenuEvent(int Entry)
	asExecuteDynamic(def.asOnMenuEvent, Info->hEntry);
}

DLL_EXPORT bool pluginit(PLUG_INITSTRUCT *InitStruct)
{
	InitStruct->pluginVersion	= PLUGIN_VERSION;
	InitStruct->sdkVersion		= PLUG_SDKVERSION;
	pluginHandle				= InitStruct->pluginHandle;
	strcpy_s(InitStruct->pluginName, PLUGIN_NAME);

	// Initialize the scripting engine
	char dir[MAX_PATH];
	GetCurrentDirectoryA(sizeof(dir), dir);
	strcat_s(dir, "\\test.as");
	
	if (!Script::EngineInit())
	{
		_plugin_printf("Script engine initialization failed!\n");
		return false;
	}

	Script::EngineLoad(&def, dir);

	// Add all of the callbacks
	_plugin_registercallback(pluginHandle, CB_INITDEBUG,			(CBPLUGIN)InitDebugCallback);
	_plugin_registercallback(pluginHandle, CB_STOPDEBUG,			(CBPLUGIN)StopDebugCallback);
	_plugin_registercallback(pluginHandle, CB_CREATEPROCESS,		(CBPLUGIN)CreateProcessCallback);
	_plugin_registercallback(pluginHandle, CB_EXITPROCESS,			(CBPLUGIN)ExitProcessCallback);
	_plugin_registercallback(pluginHandle, CB_CREATETHREAD,			(CBPLUGIN)CreateThreadCallback);
	_plugin_registercallback(pluginHandle, CB_EXITTHREAD,			(CBPLUGIN)ExitThreadCallback);
	_plugin_registercallback(pluginHandle, CB_SYSTEMBREAKPOINT,		(CBPLUGIN)SystemBreakpointCallback);
	_plugin_registercallback(pluginHandle, CB_LOADDLL,				(CBPLUGIN)LoadDllCallback);
	_plugin_registercallback(pluginHandle, CB_UNLOADDLL,			(CBPLUGIN)UnloadDllCallback);
	_plugin_registercallback(pluginHandle, CB_OUTPUTDEBUGSTRING,	(CBPLUGIN)DebugStringCallback);
	_plugin_registercallback(pluginHandle, CB_EXCEPTION,			(CBPLUGIN)ExceptionCallback);
	_plugin_registercallback(pluginHandle, CB_BREAKPOINT,			(CBPLUGIN)BreakpointCallback);
	_plugin_registercallback(pluginHandle, CB_PAUSEDEBUG,			(CBPLUGIN)PauseCallback);
	_plugin_registercallback(pluginHandle, CB_RESUMEDEBUG,			(CBPLUGIN)ResumeCallback);
	_plugin_registercallback(pluginHandle, CB_STEPPED,				(CBPLUGIN)SteppedCallback);
	_plugin_registercallback(pluginHandle, CB_ATTACH,				(CBPLUGIN)AttachCallback);
	_plugin_registercallback(pluginHandle, CB_DETACH,				(CBPLUGIN)DetachCallback);
	_plugin_registercallback(pluginHandle, CB_DEBUGEVENT,			(CBPLUGIN)DebugEventCallback);
	_plugin_registercallback(pluginHandle, CB_MENUENTRY,			(CBPLUGIN)MenuEntryCallback);

	return true;
}

DLL_EXPORT bool plugstop()
{
	_plugin_menuclear(hMenu);

	return true;
}

DLL_EXPORT void plugsetup(PLUG_SETUPSTRUCT *SetupStruct)
{
	hwndDlg = SetupStruct->hwndDlg;
	hMenu	= SetupStruct->hMenu;

	// Initialize the menu
	_plugin_menuaddentry(hMenu, 0, "Load script");

	// void OnPluginSetup(int DebuggerVersion, int PluginVersion)
	asExecuteDynamic(def.asOnPluginSetup, DBG_VERSION, PLUGIN_VERSION);
}